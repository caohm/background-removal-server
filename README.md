# Keras Model serving with CI and Heroku

Check out the blogpost [here](https://medium.com/p/35648f9dc5fb)

## Build docker image
```
    # Prepare for heroku container deployment
    docker build -t registry.gitlab.com/fast-science/background-removal-server .
    # Or for gitlab
    docker build -t registry.gitlab.com/fast-science/background-removal-server .
```

## Deploying to a docker machine

```
    docker login
    # For plain HTTP
    docker run -t -i -p80:5001 -ePORT=5001 registry.gitlab.com/fast-science/background-removal-server
    # or with https
    docker run -ti -p443:5001 -ePORT=5001 -v"$PWD/certificate:/certificate:ro" --entrypoint "gunicorn --threads 20 --bind 0.0.0.0:$PORT --timeout 120 wsgi --certfile=/certificate/server.crt --keyfile=/certificate/server.key" registry.gitlab.com/fast-science/background-removal-server
    docker run -ti -p443:5001 -v"$PWD/certificate:/certificate:ro" registry.gitlab.com/fast-science/background-removal-server gunicorn --threads 20 --bind 0.0.0.0:5001 --timeout 120 wsgi --certfile=/certificate/server.crt --keyfile=/certificate/server.key
```




## api

接口地址: http://ip:port/api/predict
请求方式: POST
数据类型: JSON
    data = {
        "threshold": 0.7,  // 0 ~ 1 
        "app_key": "a2ffc7fd859a1267da524ce6efb58f8bd0b68247",
        "rgb": {"r": 255, "g": 100, "b": 10},
        "file": base64
        }
响应类型: JSON
    response = {
        "code": 200,       // 200 or 400
        "message": "ok",   // ok or error
        "data": base64     
        }
