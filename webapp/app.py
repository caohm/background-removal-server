import base64
import io
import json
import re
import traceback

import numpy as np
import tensorflow as tf
from PIL import Image, ExifTags
from cStringIO import StringIO
from flask import Flask, request
from flask import send_file
from flask_mako import MakoTemplates, render_template
from keras.models import load_model
from plim import preprocessor
from scipy.misc import imresize

app = Flask(__name__, instance_relative_config=True)
# For Plim templates
mako = MakoTemplates(app)
app.config['MAKO_PREPROCESSOR'] = preprocessor
app.config.from_object('config.ProductionConfig')

# Preload our model
print("Loading model")
model = load_model('./model/main_model.hdf5', compile=False)
graph = tf.get_default_graph()


def ml_predict(image):
    with graph.as_default():
        # Add a dimension for the batch
        prediction = model.predict(image[None, :, :, :])
    prediction = prediction.reshape((224, 224, -1))
    return prediction


def rotate_by_exif(image):
    try:
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation] == 'Orientation': break
        exif = dict(image._getexif().items())
        if not orientation in exif:
            return image

        if exif[orientation] == 3:
            image = image.rotate(180, expand=True)
        elif exif[orientation] == 6:
            image = image.rotate(270, expand=True)
        elif exif[orientation] == 8:
            image = image.rotate(90, expand=True)
        return image
    except:
        traceback.print_exc()
        return image


def base64_to_image(base64_str, image_path=None):
    base64_data = re.sub('^data:image/.+;base64,', '', base64_str)
    binary_data = base64.b64decode(base64_data)
    img_data = StringIO(binary_data)
    img = Image.open(img_data)
    if image_path:
        img.save(image_path)
    return img


def is_num_by_except(num):
    try:
        int(num)
        return True
    except ValueError:
        return False


def convert_2_white(image, rgb):
    if rgb["r"] == "":
        rgb["r"] = 255
    if rgb["g"] == "":
        rgb["g"] = 255
    if rgb["b"] == "":
        rgb["b"] = 255

    datas = image.getdata()
    newData = []
    for item in datas:
        if item[3] == 0:
            newData.append((rgb["r"], rgb["g"], rgb["b"]))
        else:
            newData.append(item)
    image.putdata(newData)
    return image


THRESHOLD = 0.7


@app.route('/predict', methods=['POST'])
def predict():
    # Load image
    image = request.files['file']
    image = Image.open(image)
    image = rotate_by_exif(image)
    resized_image = imresize(image, (224, 224)) / 255.0

    # Model input shape = (224,224,3)
    # [0:3] - Take only the first 3 RGB channels and drop ALPHA 4th channel in case this is a PNG
    prediction = ml_predict(resized_image[:, :, 0:3])
    print('PREDICTION COUNT', (prediction[:, :, 1] > 0.5).sum())

    # Resize back to original image size
    # [:, :, 1] = Take predicted class 1 - currently in our model = Person class. Class 0 = Background
    prediction = imresize(prediction[:, :, 1], (image.height, image.width))
    prediction[prediction > THRESHOLD * 255] = 255
    prediction[prediction < THRESHOLD * 255] = 0

    # Append transparency 4th channel to the 3 RGB image channels.
    transparent_image = np.append(np.array(image)[:, :, 0:3], prediction[:, :, None], axis=-1)
    transparent_image = Image.fromarray(transparent_image)

    # Send back the result image to the client
    byte_io = io.BytesIO()
    transparent_image.save(byte_io, 'PNG')
    byte_io.seek(0)
    return send_file(byte_io, mimetype='image/png')


@app.route('/api/predict', methods=['POST'])
def api_predict():
    data = request.data
    j_data = json.loads(data)

    try:
        rgb = j_data["rgb"]
    except Exception:
        rgb = {"r": 255, "g": 255, "b": 255}

    if "a2ffc7fd859a1267da524ce6efb58f8bd0b68247" != j_data["app_key"]:
        return json.dumps({'code': 400, 'message': 'app_key error', 'data': ''})

    try:
        threshold = j_data['threshold']
        int(threshold)
    except Exception:
        try:
            float(threshold)
        except Exception:
            threshold = THRESHOLD

    try:
        image = base64_to_image(j_data['file'])
    except Exception:
        traceback.print_exc()
        return json.dumps({'code': 400, 'message': 'file error', 'data': ''})

    try:
        resized_image = imresize(image, (224, 224)) / 255.0
        # Model input shape = (224,224,3)
        # [0:3] - Take only the first 3 RGB channels and drop ALPHA 4th channel in case this is a PNG
        prediction = ml_predict(resized_image[:, :, 0:3])
        print('PREDICTION COUNT', (prediction[:, :, 1] > 0.5).sum())
        # Resize back to original image size
        # [:, :, 1] = Take predicted class 1 - currently in our model = Person class. Class 0 = Background
        prediction = imresize(prediction[:, :, 1], (image.height, image.width))
        prediction[prediction > threshold * 255] = 255
        prediction[prediction < threshold * 255] = 0
        # Append transparency 4th channel to the 3 RGB image channels.
        transparent_image = np.append(np.array(image)[:, :, 0:3], prediction[:, :, None], axis=-1)
        transparent_image = Image.fromarray(transparent_image)
        transparent_image = convert_2_white(transparent_image, rgb)
        # Send back the result image to the client
        byte_io = io.BytesIO()
        transparent_image.save(byte_io, 'PNG')
        byte_io.seek(0)
    except Exception:
        traceback.print_exc()
        return json.dumps({'code': 400, 'message': 'predict error', 'data': ''})
    b64 = base64.b64encode(byte_io.getvalue()).decode()
    return json.dumps({'code': 200, 'message': 'ok', 'data': b64})


@app.route('/')
def homepage():
    return render_template('index.html.slim', name='mako')


if __name__ == '__main__':
    app.run(host='0.0.0.0')
