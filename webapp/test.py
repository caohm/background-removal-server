import base64
import json
import re
from io import BytesIO

import requests
from PIL import Image


def base64_to_image(base64_str, image_path=None):
    base64_data = re.sub('^data:image/.+;base64,', '', base64_str)
    byte_data = base64.b64decode(base64_data)
    image_data = BytesIO(byte_data)
    img = Image.open(image_data)
    if image_path:
        img.save(image_path)
    return img


url = "http://192.168.99.100/api/predict"
with open('test.jpg', 'rb') as f:
    pic = f.read()
data = {
    "threshold": 0.3,
    "app_key": "a2ffc7fd859a1267da524ce6efb58f8bd0b68247",
    "file": base64.b64encode(pic).decode()
}
data_json = json.dumps(data)
response = requests.post(
    url=url,
    data=data_json
)

dd = json.loads(response.content.decode())
print('code', dd['code'])
print('message', dd['message'])

image = base64_to_image(dd['data'], 'out.png')
image.show()
image.close()
