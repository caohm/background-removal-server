from io import BytesIO

from PIL import Image

with open('out.png', 'rb') as f:
    pic = f.read()

image_data = BytesIO(pic)
image = Image.open(image_data)

datas = image.getdata()
newData = []
for item in datas:
    if item[3] == 0:
        newData.append((255, 255, 255))
    else:
        newData.append(item)

image.putdata(newData)
image.save("convert.png", "PNG")
image.show()
image.close()
