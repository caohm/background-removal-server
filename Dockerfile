FROM heroku/miniconda

# Grab requirements.txt.
ADD ./webapp/requirements.txt /tmp/requirements.txt

# Install dependencies
RUN pip install -qr /tmp/requirements.txt

RUN conda install scikit-learn

#RUN pip install -U numpy

# Add our code
ADD ./webapp /opt/webapp/
WORKDIR /opt/webapp



CMD gunicorn --threads 20 --bind 0.0.0.0:$PORT --timeout 120 wsgi